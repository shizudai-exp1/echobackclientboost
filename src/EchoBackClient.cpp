/*
 * EchoBackClient.cpp
 *
 *  Created on: 2016/03/07
 *      Author: yasuh
 */
#include <iostream>
#include "EchoBackClient.hpp"
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/range/algorithm_ext/erase.hpp>
#include <boost/algorithm/string/classification.hpp>

EchoBackClient::EchoBackClient() {
}

EchoBackClient::~EchoBackClient() {
}

void EchoBackClient::start(string host, string port) {

	try {
		io_service io_service;
		ip::tcp::socket socket(io_service);
		ip::tcp::resolver resolver(io_service);
		ip::tcp::resolver::query query(ip::tcp::v4(), host, port);
		ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
		socket.connect(*endpoint_iterator);

		string user_input;
		boost::array<char, 256> recv_message;
		boost::system::error_code error;

		while (true) {
			getline(cin, user_input);
			boost::remove_erase_if(user_input, boost::is_any_of("\r\n"));	// remove [CR][LF]
			socket.write_some(buffer(user_input, user_input.size()));

			sleep(1);	// read socket after 1 sec
			size_t size = socket.read_some(boost::asio::buffer(recv_message), error);
			if (error == boost::asio::error::eof) {
				break;
			} else if (error) {
				throw boost::system::system_error(error);
			}
			cout.write(recv_message.data(), size);
		}
		socket.close();
	} catch (std::exception& e) {
		cerr << e.what() << endl;
	}
}
