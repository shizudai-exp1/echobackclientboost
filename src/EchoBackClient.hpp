/*
 * EchoBackClient.hpp
 *
 *  Created on: 2016/03/07
 *      Author: yasuh
 */

#ifndef ECHOBACKCLIENT_HPP_
#define ECHOBACKCLIENT_HPP_

#include <stdint.h>
using namespace std;

#include <boost/asio.hpp>
using namespace boost;
using namespace boost::asio;

class EchoBackClient {

public:
	EchoBackClient();
	virtual ~EchoBackClient();
	void start( std::string host, string port );
};

#endif /* ECHOBACKCLIENT_HPP_ */
