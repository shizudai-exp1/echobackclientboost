//============================================================================
// Name        : Main.cpp
// Author      : Yasuhiro Noguchi
// Version     : 0.0.1
// Copyright   : GPLv2
// Description : EchoBackClient
//============================================================================
#include <iostream>
using namespace std;

#include <boost/asio.hpp>
using namespace boost::asio;

#include "EchoBackClient.hpp"

int main(int argc, char** argv) {

	EchoBackClient c;
	c.start("localhost", "8000");

	return 0;
}
